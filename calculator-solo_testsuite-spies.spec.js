
/**
 * the test spec shows usage of: 
 * Solo TEST SUITE
 * commonly used SPIES
 */

describe('Calculator', function() {
    it('UpdateResult function called during valid operation', function() {
        spyOn(window, 'updateResult');
        Calculate('3+3');
        expect(window.updateResult).toHaveBeenCalledTimes(1);
        expect(window.updateResult).toHaveBeenCalledWith(6);
    });

    it('UpdateResult function called during inappropriate operation', function() {
        spyOn(window, 'updateResult');
        Calculate('a+3');
        expect(window.updateResult).toHaveBeenCalledTimes(1);
        expect(window.updateResult).toHaveBeenCalledWith('operation not recognised');
    });

    it('Addition prototype function is called after Calculate function', function() {
        const spy = spyOn(Calculator.prototype , 'add');
        Calculate('2+3');
        expect(spy).toHaveBeenCalledTimes(2);
    })

    xit('calls subtract', function() {
        // write spy on subtract prototype method
        // call relevant operation method
        // check expectations
    });
    
    xit('calls divide', function() {
        // write spy on subtract prototype method
        // call relevant operation method
        // check expectations
    });

    xit('calls multiply', function() {
        // write spy on subtract prototype method
        // call relevant operation method
        // check expectations
    });

    it('call updateResult with callThrough', function() {
        // callThrough is used to call actual implementation
        spyOn(window, 'updateResult');
        spyOn(Calculator.prototype, 'add').and.callThrough();
        Calculate('3+4');
        expect(window.updateResult).toHaveBeenCalledWith(7);
    });

    it('call updateResult with callFake', function() {
        // callFake is used to replace actual implementation with fake implementation
        spyOn(window, 'updateResult');
        spyOn(Calculator.prototype, 'add').and.callFake(function(number) {
            return 'happiness !!!';
        });
        Calculate('3+4');
        expect(window.updateResult).toHaveBeenCalledWith('happiness !!!');
    });

    it('call updateResult with returnValue', function() {
        // returnValue is used to mock return value at actual implementation fn
        spyOn(window, 'updateResult');
        spyOn(Calculator.prototype, 'multiply').and.returnValue(21);
        Calculate('3*4');
        expect(window.updateResult).toHaveBeenCalledWith(21);
    });

    it('call updateResult with returnValues', function() {
        // returnValues is used to mock series of return values from actual implementation fn
        spyOn(window, 'updateResult');
        spyOn(Calculator.prototype, 'add').and.returnValues(null, 3);
        
        Calculate('5+3');
        expect(window.updateResult).toHaveBeenCalledWith(8);
    });

    it('Check to see if Calculate method reacts to error thrown from multiply method', function() {
        spyOn(Calculator.prototype, 'multiply').and.throwError('some error related to multiply fn');
        expect(function() { Calculate('5*5') }).toThrowError('some error related to multiply fn')
    });
});