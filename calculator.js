/**
 *  Calculator constructor function
 *  when called with the "new" operator, a new Calculator object is created
 */
function Calculator() {
    this.total = 0;
}

/**
 * Below are extended methods as part of Calculator's operations
 */
Calculator.prototype.add = function(number) {
    return this.total+= number;
}

Calculator.prototype.subtract = function(number) {
    try {
        if(this.total > 0) {
            return this.total-= number;
        } else {
            throw new Error('-ve results');
        }
    } catch(e) {
        console.log(e);
    }
}

Calculator.prototype.multiply = function(number) {
    return this.total*= number;
}

Calculator.prototype.divide = function(number) {
    if(number === 0) {
        throw new Error('divide by zero error');
    }
    return this.total/= number;
}

// creating a getter named 'version'
Object.defineProperty(Calculator.prototype, 'version', {
    get: function() {
        return '0.1';
    },
    enumerable: true,
    configurable: true
})