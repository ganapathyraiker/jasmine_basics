/**
 * @param {string} inputValue Eg: '1+2' or '3+6' or '9+1'  
 */
function Calculate(inputValue) {
    const expression = /\+|\-|\*|\//; // regular expression used below to split numbers and match operator
    const numbers = inputValue.split(expression);
    const num1 = parseInt(numbers[0]);
    const num2 = parseInt(numbers[1]);
    const operation = inputValue.match(expression);
    this.result;

    if(operation === null || Number.isNaN(num1) || Number.isNaN(num2)) {
        this.result = 'operation not recognised';
        updateResult(this.result);
        return;
    }

    const calci = new Calculator();
    calci.add(num1);
    switch(operation[0]) {
        case '+': 
                this.result = calci.add(num2);
            break;
        case '-':
                this.result = calci.subtract(num2);
            break;
        case '*': 
                this.result = calci.multiply(num2);
            break;
        case '/': 
            try {
                this.result = calci.divide(num2);
            } catch(e) {
                this.result = e;
            }
            break;
    }
    updateResult(this.result);   
}

/**
 * @param {string} result is displayed at markup
 */
function updateResult(result) {
    const element = document.getElementById('result')
    if(element) {
        element.innerText = result;
    }
}

/**
 * @param none
 */
function showVersion() {
    const calculator = new Calculator();
    const element = document.getElementById('version')
    if(element) {
        element.innerText = calculator.version;
    }
}

/**
 * reads the version from getter, created as part of Calculator
 */
// window.onload = function() {
//     showVersion();
// }