
# Simple calculator

  

This repository contains simple setup of jasmine along with custom implemented calculator using javascript.

**The jasmine core files that are included in index.html file are taken from** - [https://github.com/jasmine/jasmine/releases](https://github.com/jasmine/jasmine/releases)



**What are included ?**

 1. Html file with basic markup for calculator ( index.html )
 2. Calculator constructor function along with extended operation methods Eg add, subtract, divide etc. ( calculator.js )
 3. Calculate operation i.e exposed to mark up ( main.js )
 4. jasmine core files ( /lib )
 5. Already written test spec samples ( calculator-nestsuite-matchers.spec.js, calculator-nestsuite-matchers.spec.js and calculator-solo_testsuite-unittests.spec.js )
 6. User defined written matcher function ( custom-matchers.js ), that could be registered with jasmine and used as required. 
 7. styles to support calculator ( style.css )



**How to include and execute test spec file ?**
Including them at html file under comment

     <!-- include spec file here -->


 
**How to run the setup ?** 
Either open the html file in normal browser or run a live reloading server at root folder



**Courtesy - Unit testing your javascript with jasmine by Juan Lizarazo, at Udemy**
