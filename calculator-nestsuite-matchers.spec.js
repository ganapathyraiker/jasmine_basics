
/**
 * the test spec shows usage of: 
 * nested TEST SUITE
 * commonly used matchers Eg: toEqual, toBeTruthy etc
 */

describe('Calculator', function() {
    it('Equality between constructors', function() {
        // toEqual deep equality
        const calculator1 = new Calculator();
        const calculator2 = new Calculator();
        expect(calculator1).toEqual(calculator2);
    });

    it('Calculator can be instantiated', function() {
        // toBeTruthy toBefalsy toContain
        const calculator = new Calculator();
        expect(calculator).toBeTruthy();
        expect(calculator.constructor.name).toContain('Calc');
    });

    it('Has common operations like add and subtract', function() {
        // toBeUndefined, toBeUndefined
        const calculator = new Calculator();
        expect(calculator.add).not.toBeUndefined();
        expect(calculator.subtract).toBeDefined();
    });
    
    it('Multiply with non number parameter', function() {
        // toBeNaN
        const calculator = new Calculator();
        calculator.total = 1;
        expect(calculator.multiply('a')).toBeNaN();
    });
    
    it('To check if divide by zero error-throw is handled', function() {
        // toThrow toThrowError
        const calculator = new Calculator();
        // calculator.total = 1;
        expect(function() {calculator.divide(0)}).toThrow();
        expect(function() { calculator.divide(0) }).toThrowError(Error, 'divide by zero error')
    });

    it('The result must be number', function() {
        // toMatch
        const regularExpression = /-?\d+/;
        const calculator = new Calculator();
        calculator.total = 1;
        expect(calculator.add(2)).toBe(3);
        expect(calculator.multiply(1)).toMatch(regularExpression);
    });

    it('To be instance of calculator', function() {
        // write custom matchers 
        jasmine.addMatchers(customMatchers); // register custom writtem matcher
        const calculator = new Calculator();
        //expect(2).toBeCalculator(); // results in error
        //expect(calculator).not.toBeCalculator(); // results in error
        expect(calculator).toBeCalculator();
    });

    describe('add() oeprations', function() {
        it('Should add numbers', function() {
            let calculateFn = new Calculate('1+2'); // initialise calculator instance
            expect(calculateFn.result).toBe(3);
        });
    });
    
    describe('subtract() oeprations', function() {
        it('Should subtract numbers', function() {
            let calculateFn = new Calculate('3-1');
            expect(calculateFn.result).toBe(2);
        });
    });

    describe('multiply() oeprations', function() {
        it('Should multiple numbers', function() {
            let calculateFn = new Calculate('1*2');
            expect(calculateFn.result).toBe(2);
        });
    });

    describe('divide() oeprations', function() {
        it('Should divide numbers', function() {
            let calculateFn = new Calculate('1/2');
            expect(calculateFn.result).toBe(0.5);
        });
    });
});