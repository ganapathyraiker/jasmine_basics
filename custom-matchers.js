
/**
 * jasmine allows to write our own matcher functions
 */

const customMatchers = {
    /**
     * Custom matcher function to validate instance
     */
    toBeCalculator:  function() {
        return {
            compare: function(actual) {
                const result = {
                    pass: actual instanceof Calculator,
                    message: ''
                }

                if(result.pass) {
                    result.message = `Expected ${actual} not to be instance of calculator.`
                } else {
                    result.message = `Expected ${actual} to be instance of calculator.`
                }

                return result;
            }
        }
    },

    // continue to write your custom matcher functions ...
    //...
};